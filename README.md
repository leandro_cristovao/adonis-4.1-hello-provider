# HelloProvider

Estrutura simples de como criar um Provider para o Adonisjs 4.1

## Configuração

Adicionar na sessão `providers`, no arquivo `start/app.js` a entrada a seguir:

    const providers = [
    ...
    'hello-provider/providers/HelloProvider'
    ]

## Como usar

No próprio terminal, executar o comando `adonis repl` e em seguida, adicionar as entradas abaixo

    const Hello = use('Adonis/Addons/Hello')
    Hello.sayHello('Leandro')

## Teste

    npm test
