const { test } = require('@japa/runner')

test.group('HelloProvider', () => {
  test('Teste de chamada do método sayHello(name)', async ({ assert }) => {
    const Hello = require('../src/Hello')
    const hello = new Hello()

    const result = hello.sayHello('Adonis')
    assert.equal(result, 'Hello Adonis')
  })
})
