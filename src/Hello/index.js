
class Hello {
  constructor (Config) {
    this.Config = Config
  }

  sayHello (name) {
    return `Hello ${name}`
  }
}

module.exports = Hello
