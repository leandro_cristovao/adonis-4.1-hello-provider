const { ServiceProvider } = require('@adonisjs/fold')

class HelloProvider extends ServiceProvider {
  register () {
    this.app.singleton('Adonis/Addons/Hello', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('../src/Hello'))(Config)
    })
  }
}

module.exports = HelloProvider
